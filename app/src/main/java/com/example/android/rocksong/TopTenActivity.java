package com.example.android.rocksong;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class TopTenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.song_list);

        //Create a list of songs.
        ArrayList<Song> songs = new ArrayList<>();
        songs.add(new Song("Queen", "Bohemian Rhapsody", "1975","A Night at the Opera",R.drawable.cover1));
        songs.add(new Song("Led Zeppelin", "Stairway to Heaven", "1971","Led Zeppelin IV",R.drawable.cover2));
        songs.add(new Song("Nirvana", "Smells Like Teen Spirit", "1991","Nevermind",R.drawable.cover3));
        songs.add(new Song("Deep Purple", "Smoke On The Water" , "1972","Machine Head",R.drawable.cover4));
        songs.add(new Song("Metallica", "Enter Sandman","1991","Metallica",R.drawable.cover5));
        songs.add(new Song("Aerosmith", "Dream On", "1973","Aerosmith",R.drawable.cover6));
        songs.add(new Song("AC/DC", "Thunderstruck", "1990","The Razor's Edge",R.drawable.cover7));
        songs.add(new Song("Guns N'Roses", "Sweet Child O'Mine", "1987","Appetite for Destruction",R.drawable.cover8));
        songs.add(new Song("Led Zeppelin", "Whole Lotta Love","1969","Led Zeppelin II",R.drawable.cover9));
        songs.add(new Song("Steppenwolf", "Born To Be Wild","1968","Steppenwolf",R.drawable.cover10));


        SongAdapter adapter = new SongAdapter(this, songs);

        ListView listView = (ListView) findViewById(R.id.list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(TopTenActivity.this, SingleSongActivity.class);

                i.putExtra("song", (Song)((ListView) parent).getAdapter().getItem(position));

                startActivity(i);
            }
        });

        listView.setAdapter(adapter);

    }
}
