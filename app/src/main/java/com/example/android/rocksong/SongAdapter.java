package com.example.android.rocksong;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public class SongAdapter extends ArrayAdapter<Song> {

    public SongAdapter(Context context, ArrayList<Song> songs) {
        super(context,0,songs);
    }

    @Override
    public View getView(int position, View convertView,ViewGroup parent) {
        //Check if an existing view is being reused,otherwise inflate the view.
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item,parent,false);
        }
        //Get the Song object located at this position in the list.
        Song currentSong = getItem(position);

        //Find the TextView in the list_item.xml layout with the ID name_text_view.
        TextView nameTextView = (TextView) listItemView.findViewById(R.id.name_text_view);
        //Get the artist name from the currentSong object and set this text on the name TextView.
        nameTextView.setText(currentSong.getArtistName());

        //Find the TextView in the list_item.xml layout with the ID title_text_view.
        TextView titleTextView = (TextView) listItemView.findViewById(R.id.title_text_view);
        //Get the song title from the currentSong object and set this text on the title TextView.
        titleTextView.setText(currentSong.getSongTitle());


        //Return the whole list item layout (containing 2 TextViews) so that it can be shown in the ListView.
        return listItemView;
    }
}
