package com.example.android.rocksong;

import java.io.Serializable;

public class Song implements Serializable {
    //Name of the artist or band.
    private String artistName;

    //Song title.
    private String songTitle;

    //Year of album release.
    private String year;

    //Name of album.
    private String albumName;


    private int resourceCoverId;


    //Create a new Song object.
    public Song(String artistName, String songTitle, String year, String albumName, int resourceCoverId) {
        this.artistName = artistName;
        this.songTitle = songTitle;
        this.year = year;
        this.albumName = albumName;
        this.resourceCoverId = resourceCoverId;
    }

    //Get the artist name.
    public String getArtistName() {
        return artistName;
    }

    //Get the song title.
    public String getSongTitle() {
        return songTitle;
    }

    public String getYearOfRelease() {
        return year;
    }

    public String getAlbumName() {
        return albumName;
    }
    //Get resource cover id.
    public int getResourceCoverId() {
        return resourceCoverId;
    }
}
