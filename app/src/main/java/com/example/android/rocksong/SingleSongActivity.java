package com.example.android.rocksong;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class SingleSongActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.song);

        final Song song = (Song) getIntent().getSerializableExtra("song");

        TextView nameTextView = (TextView) findViewById(R.id.name_text_view);
        nameTextView.setText(song.getArtistName());

        TextView titleTextView = (TextView) findViewById(R.id.title_text_view);
        titleTextView.setText(song.getSongTitle());

        TextView yearTextView = (TextView) findViewById(R.id.year_text_view);
        yearTextView.setText(song.getYearOfRelease());

        TextView albumTextView = (TextView) findViewById(R.id.album_text_view);
        albumTextView.setText(song.getAlbumName());

        Button coverButton = (Button) findViewById(R.id.cover_button);
        coverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SingleSongActivity.this, CoverActivity.class);

                i.putExtra("cover",song.getResourceCoverId());

                startActivity(i);
            }
        });
    }

}
