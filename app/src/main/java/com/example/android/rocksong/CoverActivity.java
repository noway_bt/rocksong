package com.example.android.rocksong;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

public class CoverActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cover);
        //
        int coverId = getIntent().getIntExtra("cover", R.drawable.cover1);
        ImageView cover = (ImageView) findViewById(R.id.coverView);
        cover.setImageResource(coverId);


    }
}
